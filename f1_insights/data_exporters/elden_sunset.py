from mage_ai.io.file import FileIO
from pyspark.sql import DataFrame

if 'data_exporter' not in globals():
    from mage_ai.data_preparation.decorators import data_exporter


@data_exporter
def export_data_to_file(df: DataFrame, **kwargs) -> None:
    """
    Template for exporting data to filesystem.

    Docs: https://docs.mage.ai/design/data-loading#fileio
    """
    spark = kwargs.get('spark')
    
    filepath = 'historical/sessions'
    #FileIO().export(df, filepath)
    df.write.csv(filepath)
