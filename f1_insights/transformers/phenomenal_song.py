from pyspark.sql import DataFrame
if 'transformer' not in globals():
    from mage_ai.data_preparation.decorators import transformer
if 'test' not in globals():
    from mage_ai.data_preparation.decorators import test


@transformer
def transform(input_df: DataFrame, *args, **kwargs) -> DataFrame:

    output_df = input_df.where("location = 'Barcelona'")

    return output_df


@test
def test_output(output, *args) -> None:
    """
    Template code for testing the output of the block.
    """
    assert output is not None, 'The output is undefined'
